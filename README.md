# Kuehne-Nage test task
The application is made of Java 8, Maven and JUnit. No any other external libraries were used.

By default the application reads `results.csv` file and writes to `results.xml`.  
Files can be changed by command line arguments. 

The rules for the point calculation can be found here: https://en.wikipedia.org/wiki/Decathlon#Points_system


### How to run unit tests
```mvn test```


### How to execute application
```mvn exec:java```  
or  
```mvn exec:java -Dexec.args="inputFile outputFile"```

