package kune.decathlon;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static kune.decathlon.Event.*;
import static org.junit.Assert.assertEquals;

public class ScoreUtilTest {

    @Test
    public void calculateScore() {
        Map<Event, Double> events = new HashMap<Event, Double>() {{
            put(RUN_100, 12.61);
            put(JUMP_LONG, 500d);
            put(SHOT_PUT, 9.22);
            put(JUMP_HIGH, 150d);
            put(RUN_400, 60.39);
            put(RUN_110_HURDLES, 16.43);
            put(THROW_DISCUS, 21.60);
            put(POLE_VAULT, 260d);
            put(THROW_JAVELIN, 35.81);
            put(RUN_1500, 325.72);
        }};

        assertEquals("Total scores calculated incorrectly", 4200, ScoreUtil.calculateScore(events));
    }

}
