package kune.decathlon;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FormatRankTest {

    @Test
    public void noSharedRanks() {
        assertEquals("1", ScoreUtil.formatRank(1, 1));
    }

    @Test
    public void twoAthletesShareRank() {
        assertEquals("3-4", ScoreUtil.formatRank(3, 2));
    }

    @Test
    public void allAthletesHaveSameRank() {
        assertEquals("1-3", ScoreUtil.formatRank(1, 3));
    }

}
