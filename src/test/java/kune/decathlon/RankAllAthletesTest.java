package kune.decathlon;

import kune.model.Athlete;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RankAllAthletesTest {

    @Test
    public void allAthletesDifferentRank() {
        List<Athlete> athletes = asList(
                new Athlete().setScore(10),
                new Athlete().setScore(20),
                new Athlete().setScore(15));

        ScoreUtil.rankAllAthletes(athletes);

        List<String> actualRanks = athletes.stream()
                .map(Athlete::getRank)
                .collect(toList());

        assertEquals(asList("3", "1", "2"), actualRanks);
    }

    @Test
    public void twoAthletesSameRank() {
        List<Athlete> athletes = asList(
                new Athlete().setScore(10),
                new Athlete().setScore(20),
                new Athlete().setScore(15),
                new Athlete().setScore(20));

        ScoreUtil.rankAllAthletes(athletes);

        List<String> actualRanks = athletes.stream()
                .map(Athlete::getRank)
                .collect(toList());

        assertEquals(asList("4", "1-2", "3", "1-2"), actualRanks);
    }

    @Test
    public void allAthletesSameRank() {
        List<Athlete> athletes = asList(
                new Athlete().setScore(25),
                new Athlete().setScore(25),
                new Athlete().setScore(25));

        ScoreUtil.rankAllAthletes(athletes);

        List<String> actualRanks = athletes.stream()
                .map(Athlete::getRank)
                .collect(toList());

        assertEquals(asList("1-3", "1-3", "1-3"), actualRanks);
    }

    @Test
    public void emptyAthleteList() {
        List<Athlete> athletes = Collections.emptyList();

        ScoreUtil.rankAllAthletes(athletes);

        List<String> actualRanks = athletes.stream()
                .map(Athlete::getRank)
                .collect(toList());

        assertTrue(actualRanks.isEmpty());
    }

}
