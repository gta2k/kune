package kune.decathlon;

import kune.model.Athlete;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import static java.util.Arrays.asList;
import static kune.decathlon.ScoreUtil.sortGroupByScore;
import static org.junit.Assert.assertEquals;

public class SortGroupByScoreTest {

    @Test
    public void allAthletesDifferentScore() {
        Athlete a1 = new Athlete().setScore(10);
        Athlete a2 = new Athlete().setScore(20);
        Athlete a3 = new Athlete().setScore(40);
        Athlete a4 = new Athlete().setScore(30);

        List<Athlete> athletes = asList(a1, a2, a3, a4);

        SortedMap<Integer, List<Athlete>> expectedScoreMap = new TreeMap<Integer, List<Athlete>>() {{
            put(40, asList(a3));
            put(30, asList(a4));
            put(20, asList(a2));
            put(10, asList(a1));
        }};

        assertEquals(expectedScoreMap, sortGroupByScore(athletes));
    }

    @Test
    public void twoAthletesSameScore() {
        Athlete a1 = new Athlete().setScore(10);
        Athlete a2 = new Athlete().setScore(20);
        Athlete a3 = new Athlete().setScore(15);
        Athlete a4 = new Athlete().setScore(20);

        List<Athlete> athletes = asList(a1, a2, a3, a4);

        SortedMap<Integer, List<Athlete>> expectedScoreMap = new TreeMap<Integer, List<Athlete>>() {{
            put(20, asList(a2, a4));
            put(15, asList(a3));
            put(10, asList(a1));
        }};

        assertEquals(expectedScoreMap, sortGroupByScore(athletes));
    }

    @Test
    public void allAthletesSameScore() {
        Athlete a1 = new Athlete().setScore(25);
        Athlete a2 = new Athlete().setScore(25);
        Athlete a3 = new Athlete().setScore(25);

        List<Athlete> athletes = asList(a1, a2, a3);

        SortedMap<Integer, List<Athlete>> expectedScoreMap = new TreeMap<Integer, List<Athlete>>() {{
            put(25, asList(a1, a2, a3));
        }};

        assertEquals(expectedScoreMap, sortGroupByScore(athletes));
    }

    @Test
    public void emptyAthleteList() {
        List<Athlete> athletes = Collections.emptyList();

        SortedMap<Integer, List<Athlete>> expectedScoreMap = new TreeMap<>();

        assertEquals(expectedScoreMap, sortGroupByScore(athletes));
    }

}
