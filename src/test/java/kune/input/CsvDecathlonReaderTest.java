package kune.input;

import kune.model.Athlete;
import org.junit.Test;

import static kune.decathlon.Event.*;
import static org.junit.Assert.assertEquals;

public class CsvDecathlonReaderTest {

    @Test
    public void parseLine() {
        CsvDecathlonReader csvParser = new CsvDecathlonReader();
        Athlete actual = csvParser.parseLine("John Smith;12.61;5.00;9.22;1.50;60.39;16.43;21.60;2.60;35.81;5.25.72");

        Athlete expected = new Athlete()
                .setName("John Smith")
                .setResult(RUN_100, 12.61)
                .setResult(JUMP_LONG, 500d)
                .setResult(SHOT_PUT, 9.22)
                .setResult(JUMP_HIGH, 150d)
                .setResult(RUN_400, 60.39)
                .setResult(RUN_110_HURDLES, 16.43)
                .setResult(THROW_DISCUS, 21.60)
                .setResult(POLE_VAULT, 260d)
                .setResult(THROW_JAVELIN, 35.81)
                .setResult(RUN_1500, 325.72);

        assertEquals("Name didn't match", expected.getName(), actual.getName());
        assertEquals("Results parsed incorrectly", expected.getResults(), actual.getResults());
    }

}