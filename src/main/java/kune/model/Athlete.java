package kune.model;

import kune.decathlon.Event;

import java.util.HashMap;
import java.util.Map;

public class Athlete {
    private String name;
    private String rank;
    private int score;
    private Map<Event, Double> results = new HashMap<>();


    public int getScore() {
        return score;
    }

    public Athlete setScore(int score) {
        this.score = score;
        return this;
    }

    public Map<Event, Double> getResults() {
        return results;
    }

    public Athlete setResult(Event event, Double result) {
        results.put(event, result);
        return this;
    }

    public String getRank() {
        return rank;
    }

    public Athlete setRank(String rank) {
        this.rank = rank;
        return this;
    }

    public String getName() {
        return name;
    }

    public Athlete setName(String name) {
        this.name = name;
        return this;
    }
}
