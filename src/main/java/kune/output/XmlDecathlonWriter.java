package kune.output;

import kune.model.Athlete;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedWriter;
import java.io.Writer;
import java.util.List;

/**
 * It would be much cleaner to use external library to generate nice XML
 * eg JAXB or XStream
 */

public class XmlDecathlonWriter extends DecathlonWriter {
    @Override
    public void writeStream(Writer outputStream, List<Athlete> athleteList) throws ParserConfigurationException, TransformerException {
        BufferedWriter writer = new BufferedWriter(outputStream);

        Document domDocument = generateDocument(athleteList);
        DOMSource domSource = new DOMSource(domDocument);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(domSource, new StreamResult(writer));
    }

    private Document generateDocument(List<Athlete> athleteList) throws ParserConfigurationException {
        Document document = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .newDocument();

        Element root = document.createElement("athletes");
        document.appendChild(root);

        athleteList.forEach(athlete -> {
            Element athleteNode = document.createElement("athlete");
            athleteNode.setAttribute("name", athlete.getName());
            athleteNode.setAttribute("rank", athlete.getRank());
            athleteNode.setAttribute("scores", String.valueOf(athlete.getScore()));
            root.appendChild(athleteNode);

            athlete.getResults().keySet().forEach(k -> {
                Element event = document.createElement("event");
                event.setAttribute(k.toString(), String.valueOf(athlete.getResults().get(k)));
                athleteNode.appendChild(event);
            });
        });

        return document;
    }
}
