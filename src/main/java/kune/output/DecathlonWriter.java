package kune.output;

import kune.model.Athlete;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.Writer;
import java.util.List;

public abstract class DecathlonWriter {
    public abstract void writeStream(Writer outputStream, List<Athlete> athletes) throws ParserConfigurationException, TransformerException;
}
