package kune.input;

import kune.model.Athlete;

import java.io.BufferedReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Double.parseDouble;
import static kune.decathlon.Event.*;

public class CsvDecathlonReader extends DecathlonReader {

    private final static String CSV_SEPARATOR = ";";

    @Override
    public List<Athlete> readStream(Reader inputStream) {
        List<Athlete> athleteList = new ArrayList<>();

        BufferedReader reader = new BufferedReader(inputStream);
        reader.lines()
                .filter(line -> !line.isEmpty())
                .forEach(line -> athleteList.add(parseLine(line)));

        return athleteList;
    }

    Athlete parseLine(String line) {
        String[] lineParts = line.split(CSV_SEPARATOR);

        return new Athlete()
                .setName(lineParts[0])
                .setResult(RUN_100, parseDouble(lineParts[1]))
                .setResult(JUMP_LONG, parseDouble(lineParts[2]) * 100) //to centimeters
                .setResult(SHOT_PUT, parseDouble(lineParts[3]))
                .setResult(JUMP_HIGH, parseDouble(lineParts[4]) * 100) //to centimeters
                .setResult(RUN_400, parseDouble(lineParts[5]))
                .setResult(RUN_110_HURDLES, parseDouble(lineParts[6]))
                .setResult(THROW_DISCUS, parseDouble(lineParts[7]))
                .setResult(POLE_VAULT, parseDouble(lineParts[8]) * 100) //to centimeters
                .setResult(THROW_JAVELIN, parseDouble(lineParts[9]))
                .setResult(RUN_1500, parseSeconds(lineParts[10]));
    }

    private Double parseSeconds(String time) {
        String[] split = time.split("[:.]", 2);
        double minutes = parseDouble(split[0]);
        double seconds = parseDouble(split[1]);
        return minutes * 60 + seconds;
    }

}
