package kune.input;

import kune.model.Athlete;

import java.io.Reader;
import java.util.List;

public abstract class DecathlonReader {
    public abstract List<Athlete> readStream(Reader inputStream);
}
