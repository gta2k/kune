package kune;

import kune.decathlon.ScoreUtil;
import kune.input.CsvDecathlonReader;
import kune.input.DecathlonReader;
import kune.model.Athlete;
import kune.output.DecathlonWriter;
import kune.output.XmlDecathlonWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.util.List;


public class App {

    private static String inputFile = "results.csv";
    private static String outputFile = "results.xml";

    public static void main(String[] args) throws TransformerException, ParserConfigurationException, IOException {
        setInputOutputFile(args);

        Reader inputStream = new FileReader(inputFile);
        Writer outputStream = new FileWriter(outputFile);

        DecathlonReader decathlonReader = new CsvDecathlonReader();
        List<Athlete> athletes = decathlonReader.readStream(inputStream);

        for (Athlete athlete : athletes) {
            int score = ScoreUtil.calculateScore(athlete.getResults());
            athlete.setScore(score);
        }

        ScoreUtil.rankAllAthletes(athletes);

        DecathlonWriter decathlonWriter = new XmlDecathlonWriter();
        decathlonWriter.writeStream(outputStream, athletes);
    }

    private static void setInputOutputFile(String[] args) {
        if (args.length > 0) {
            inputFile = args[0];
        }
        if (args.length > 1) {
            outputFile = args[1];
        }
    }
}
