package kune.decathlon;

import kune.model.Athlete;

import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Stream;

import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class ScoreUtil {

    public static int calculateScore(Map<Event, Double> results) {
        return Stream.of(Event.values())
                .mapToInt(e -> e.type.formula.calculate(e.A, e.B, e.C, results.get(e)))
                .sum();
    }

    public static void rankAllAthletes(List<Athlete> athletes) {
        SortedMap<Integer, List<Athlete>> scoreMap = ScoreUtil.sortGroupByScore(athletes);

        int[] rank = {1};
        scoreMap.forEach((score, athleteList) -> {
            athleteList.forEach(athlete -> athlete.setRank(formatRank(rank[0], athleteList.size())));
            rank[0] += athleteList.size();
        });

    }

    // score -> List of athletes with same score
    static SortedMap<Integer, List<Athlete>> sortGroupByScore(List<Athlete> athletes) {
        return athletes.stream()
                .collect(groupingBy(
                        Athlete::getScore, () -> new TreeMap<>(reverseOrder()), toList()
                ));
    }

    static String formatRank(int rank, int numberOfAthletes) {
        if (numberOfAthletes == 1) {
            return String.valueOf(rank);
        }
        return String.format("%s-%s", rank, (rank + numberOfAthletes - 1));
    }

}
