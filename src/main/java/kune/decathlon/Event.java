package kune.decathlon;

import static kune.decathlon.EventType.FIELD_EVENT;
import static kune.decathlon.EventType.TRACK_EVENT;

/**
 * Event list and coefficients are taken from
 * https://en.wikipedia.org/wiki/Decathlon#Points_system
 */

public enum Event {
    RUN_100(25.4347, 18, 1.81, TRACK_EVENT),
    RUN_400(1.53775, 82, 1.81, TRACK_EVENT),
    RUN_1500(0.03768, 480, 1.85, TRACK_EVENT),
    RUN_110_HURDLES(5.74352, 28.5, 1.92, TRACK_EVENT),
    JUMP_LONG(0.14354, 220, 1.4, FIELD_EVENT),
    JUMP_HIGH(0.8465, 75, 1.42, FIELD_EVENT),
    THROW_DISCUS(12.91, 4, 1.1, FIELD_EVENT),
    THROW_JAVELIN(10.14, 7, 1.08, FIELD_EVENT),
    SHOT_PUT(51.39, 1.5, 1.05, FIELD_EVENT),
    POLE_VAULT(0.2797, 100, 1.35, FIELD_EVENT);

    public final double A;
    public final double B;
    public final double C;
    public final EventType type;

    Event(double A, double B, double C, EventType type) {
        this.A = A;
        this.B = B;
        this.C = C;
        this.type = type;
    }

}
