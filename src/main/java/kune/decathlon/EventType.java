package kune.decathlon;

/**
 * Score formulas are taken from
 * https://en.wikipedia.org/wiki/Decathlon#Points_system
 */

public enum EventType {
    TRACK_EVENT((A, B, C, P) -> Double.valueOf(A * Math.pow(B - P, C)).intValue()),
    FIELD_EVENT((A, B, C, P) -> Double.valueOf(A * Math.pow(P - B, C)).intValue());

    public final Formula formula;

    EventType(Formula formula) {
        this.formula = formula;
    }

    @FunctionalInterface
    interface Formula {
        int calculate(double A, double B, double C, double P);
    }
}
